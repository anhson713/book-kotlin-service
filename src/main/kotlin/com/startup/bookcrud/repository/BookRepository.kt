package com.startup.bookcrud.repository

import com.startup.bookcrud.entity.Book
import org.springframework.data.jpa.repository.JpaRepository

interface BookRepository : JpaRepository<Book, String> {}