package com.startup.bookcrud.configuration

import com.startup.bookcrud.repository.BookRepository
import com.startup.bookcrud.service.BookService
import com.startup.bookcrud.service.impl.BookServiceImpl
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class BookConfiguration {

    @Bean
    fun bookService(repository: BookRepository) : BookService {
        return BookServiceImpl(repository = repository)
    }
}