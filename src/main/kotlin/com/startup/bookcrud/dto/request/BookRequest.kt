package com.startup.bookcrud.dto.request

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

data class BookRequest(
    @field:NotBlank
    var name: String,
    var description: String,
    @field:NotNull
    var releaseAt: Int,
    @field:NotBlank
    var category: String
)