package com.startup.bookcrud.dto.response

import com.startup.bookcrud.constant.Category
import com.startup.bookcrud.entity.Book

data class BookResponse(
    var id: String,
    var name: String,
    var description: String,
    var releaseAt: Int,
    var category: Category
) {
    companion object {
        fun from(entity: Book): BookResponse {
            return BookResponse(
                id = entity.id,
                name = entity.name,
                description = entity.description,
                releaseAt = entity.releaseAt,
                category = entity.category
            )
        }
    }
}