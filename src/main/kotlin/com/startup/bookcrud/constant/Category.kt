package com.startup.bookcrud.constant

enum class Category {

    SELF_HELP,
    THRILLER,
    ACTION,
    ROMANCE,
    SCI_FI,
    MYSTERY,
    SHORT_STORY
}