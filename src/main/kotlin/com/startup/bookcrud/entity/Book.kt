package com.startup.bookcrud.entity

import com.startup.bookcrud.constant.Category
import com.startup.bookcrud.dto.request.BookRequest
import java.util.*
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.Id

@Entity
data class Book(
    var name: String,
    var description: String,
    var releaseAt: Int,
    @Enumerated(EnumType.STRING)
    var category: Category
) {
    @Id
    var id: String = UUID.randomUUID().toString()
    var isActive: Boolean = false

    companion object {
        fun from(request: BookRequest): Book {
            return Book(
                name = request.name,
                description = request.description,
                releaseAt = request.releaseAt,
                category = Category.valueOf(request.category)
            )
        }
    }
}
