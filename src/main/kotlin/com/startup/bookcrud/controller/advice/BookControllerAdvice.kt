package com.startup.bookcrud.controller.advice

import com.startup.bookcrud.exception.BaseException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
class BookControllerAdvice {

    @ExceptionHandler
    fun handle(exception: BaseException) : ResponseEntity<Map<String, String>> {
        var errorMap: HashMap<String, String> = HashMap()
        errorMap.put("code", exception.code)
        errorMap.put("status", exception.status.toString())
        return ResponseEntity.status(HttpStatus.valueOf(exception.status)).body(errorMap)
    }

    @ExceptionHandler
    fun handle(exception: MethodArgumentNotValidException) : ResponseEntity<Map<String, String>> {
        var errorMap: HashMap<String, String> = HashMap()
        exception.bindingResult.fieldErrors.forEach { error ->
            errorMap[error.field] = error.defaultMessage.toString()
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMap)
    }
}