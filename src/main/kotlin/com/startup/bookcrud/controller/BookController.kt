package com.startup.bookcrud.controller

import com.startup.bookcrud.dto.request.BookRequest
import com.startup.bookcrud.dto.response.BookResponse
import com.startup.bookcrud.service.BookService
import mu.KotlinLogging
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import javax.validation.Valid
private val log = KotlinLogging.logger() {}
@RestController
@RequestMapping("/api/v1/books")
class BookController(val service : BookService) {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody @Valid request: BookRequest) : BookResponse {
        log.info { "(create)request : $request" }
        return service.create(request)
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun update(@PathVariable id: String, @RequestBody @Valid request: BookRequest) : BookResponse {
        log.info { "(update)id : $id, request : $request" }
        return service.update(id, request)
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun delete(@PathVariable id: String) {
        log.info { "(delete)id : $id" }
        service.delete(id)
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun get(@PathVariable id: String) : BookResponse {
        log.info { "(get)id : $id" }
        return service.get(id)
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun list() : List<BookResponse> {
        log.info { "(list)" }
        return service.list()
    }
}