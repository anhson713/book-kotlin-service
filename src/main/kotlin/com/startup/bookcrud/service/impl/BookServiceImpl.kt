package com.startup.bookcrud.service.impl

import com.startup.bookcrud.constant.Category
import com.startup.bookcrud.dto.request.BookRequest
import com.startup.bookcrud.dto.response.BookResponse
import com.startup.bookcrud.entity.Book
import com.startup.bookcrud.exception.NotFoundException
import com.startup.bookcrud.repository.BookRepository
import com.startup.bookcrud.service.BookService
import mu.KotlinLogging

private val logger = KotlinLogging.logger {}

class BookServiceImpl(val repository: BookRepository) : BookService {

    override fun create(request: BookRequest): BookResponse {
        logger.info { "(create)request : $request" }
        var book = Book.from(request)
        book = repository.save(book)
        return BookResponse.from(book)
    }

    override fun update(id: String, request: BookRequest): BookResponse {
        logger.info { "(update)id : $id, request : $request" }
        var book = repository.findById(id)
            .orElseThrow { throw NotFoundException(id, Book.javaClass.simpleName) }
        book.name = request.name
        book.description = request.description
        book.category = Category.valueOf(request.category)
        book.releaseAt = request.releaseAt
        book = repository.save(book)
        return BookResponse.from(book)
    }

    override fun delete(id: String) {
        logger.info { "(delete)id : $id" }
        if (!repository.existsById(id)) {
            throw NotFoundException(id, Book.javaClass.simpleName)
        }
        repository.deleteById(id)
    }

    override fun get(id: String): BookResponse {
        logger.info { "(get)id $id" }
        var book = repository.findById(id)
            .orElseThrow { throw NotFoundException(id, Book.javaClass.simpleName) }
        return BookResponse.from(book)
    }

    override fun list(): List<BookResponse> {
        logger.info { "(list)" }
        return repository.findAll().map { book -> BookResponse.from(book) }
    }
}