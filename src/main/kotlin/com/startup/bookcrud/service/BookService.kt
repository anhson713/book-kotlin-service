package com.startup.bookcrud.service

import com.startup.bookcrud.dto.request.BookRequest
import com.startup.bookcrud.dto.response.BookResponse
import lombok.extern.slf4j.Slf4j

@Slf4j
interface BookService {

    fun create(request : BookRequest) : BookResponse
    fun update(id : String, request: BookRequest) : BookResponse
    fun delete(id : String)
    fun get(id : String) : BookResponse
    fun list() : List<BookResponse>
}