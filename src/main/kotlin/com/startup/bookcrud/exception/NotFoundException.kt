package com.startup.bookcrud.exception

import org.springframework.http.HttpStatus

class NotFoundException(var id: String, var name: String) : BaseException(
    status = HttpStatus.NOT_FOUND.value(),
    code = "com.startup.bookcrud.exception.NotFoundException"
) {
    init {
        params.put("id", id)
        params.put("name", name)
    }
}