package com.startup.bookcrud.exception

open class BaseException(var status : Int,
                         var code : String) : RuntimeException(){
    var params : HashMap<String, String> = HashMap()
}